#!/usr/bin/env node
// @ts-check
const fs = require('fs/promises')
const axios = require('axios')
const { Command } = require('commander')
const pkg = require('../package.json')

// @ts-ignore
const api = axios.create()
const CI_VARS = {
  projectId: '',
  projectDir: '',
  branch: '',
  token: '',
}
let BASE_URL = 'https://gitlab.com/api/v4'
let EXIT_CODE = 0
let DEBUG = false

const program = new Command()
program
    .requiredOption('-t, --title <title>', 'Issue title')
    .option(
        '-u, --base-url <url>',
        'GitLab API base URL',
        'https://gitlab.com/api/v4'
    )
    .option('-m, --description <description>', 'Issue description')
    .option('-c, --confidential', 'Make issue confidential')
    .option('-d, --debug', 'debug (verbose) mode')
    .version(pkg.version)
    .usage('--title <title> [--description <description>] [--confidential] [options]')
    .parse(process.argv)
const options = program.opts()

/**
 * @param {string}  message
 * @param {any} [extra]
 */
function debug(message, extra) {
  if (DEBUG) {
    if (extra) {
      console.log(message, extra)
    } else {
      console.log(message)
    }
  }
}

/**
 * @param {string}  [reason]
 * @param {Error} [error]
 */
function exit(reason, error) {
  if (reason) {
    console.error(reason)
  }
  if (error) {
    console.error(error)
  }
  process.exit(EXIT_CODE)
}

async function createIssue() {
  if (DEBUG) {
    process.stdout.write('DEBUG mode: ')
  }
  process.stdout.write(`Creating issue ` + options.title + `.. `)
  try {
    debug('api.defaults.baseURL: ', api.defaults.baseURL)

    const url = `/projects/${CI_VARS.projectId}/issues`
    const payload = {
      title: options.title,
      description: options.description || '',
      confidential: !!options.confidential,
    }

    debug('payload: ', payload)

    await api.post(url, payload)
    process.stdout.write('Done.\n')
  } catch (error) {
    if (error.response && error.response.status === 400) {
      debug('Error pushing: ', error)
      process.stdout.write(
          'Push failed with status code 400. File not modified?\n'
      )
      exit()
    } else {
      exit('Push failed', error)
    }
  }
}

;(async () => {
  DEBUG = !!options.debug
  BASE_URL = options.baseUrl || BASE_URL

  // If we're not inside a local runner
  if (DEBUG || process.env.CI_PROJECT_ID !== '0') {
    if (process.env.GL_PRIVATE_TOKEN !== undefined) {
      CI_VARS.projectId = process.env.CI_PROJECT_ID
      CI_VARS.projectDir = process.env.CI_PROJECT_DIR
      CI_VARS.branch = process.env.CI_COMMIT_BRANCH
      CI_VARS.token = process.env.GL_PRIVATE_TOKEN
      debug('CI_VARS: ', CI_VARS)
    } else {
      exit('Error: required ENV variable GL_PRIVATE_TOKEN undefined.')
    }
  }

  if (options.failOnError) {
    EXIT_CODE = 1
  }

  api.defaults.baseURL = BASE_URL
  api.defaults.headers.common['Private-Token'] = CI_VARS.token
  debug('api.defaults: ', api.defaults)

  createIssue()
})()
