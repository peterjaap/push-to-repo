#!/usr/bin/env node
// @ts-check
const fs = require('fs/promises')
const axios = require('axios')
const { Command } = require('commander')
const pkg = require('../package.json')

// @ts-ignore
const api = axios.create()
const CI_VARS = {
  projectId: '',
  projectDir: '',
  branch: '',
  token: '',
}
let BASE_URL = 'https://gitlab.com/api/v4'
let FILE_NAME = ''
let COMMIT_MESSAGE = ''
let EXIT_CODE = 0
let DEBUG = false
let BRANCH = ''

const program = new Command()
program
  .requiredOption('-f, --file-name <filename>', 'the file to push')
  .option('-b, --branch <branch>', 'the branch to push', 'CI_COMMIT_BRANCH')
  .option(
    '-m, --message <message>',
    'commit message',
    'Update <filename> [skip ci]'
  )
  .option(
    '-u, --base-url <url>',
    'GitLab API base URL',
    'https://gitlab.com/api/v4'
  )
  .option('-n, --author-name <name>', 'Author name')
  .option('-e, --author-email <email>', 'Author e-mail')
  .option('--fail-on-error', 'fail the job on error')
  .option('-d, --debug', 'debug (verbose) mode')
  .version(pkg.version)
  .usage('-f <file_path> [options]')
  .parse(process.argv)
const options = program.opts()

/**
 * @param {string}  message
 * @param {any} [extra]
 */
function debug(message, extra) {
  if (DEBUG) {
    if (extra) {
      console.log(message, extra)
    } else {
      console.log(message)
    }
  }
}

/**
 * @param {string}  [reason]
 * @param {Error} [error]
 */
function exit(reason, error) {
  if (reason) {
    console.error(reason)
  }
  if (error) {
    console.error(error)
  }
  process.exit(EXIT_CODE)
}

async function pushFile() {
  if (DEBUG) {
    process.stdout.write('DEBUG mode: ')
  }
  process.stdout.write(`Pushing ${FILE_NAME} updates to ${BRANCH}.. `)
  try {
    const filePath = `${CI_VARS.projectDir}/${FILE_NAME}`
    const fileUrl = encodeURIComponent(FILE_NAME)

    debug('filePath: ', filePath)
    debug('fileUrl: ', fileUrl)
    debug('api.defaults.baseURL: ', api.defaults.baseURL)

    const fileContents = await fs.readFile(filePath, 'utf8')
    const url = `/projects/${CI_VARS.projectId}/repository/files/${fileUrl}`
    const payload = {
      branch: BRANCH,
      content: fileContents,
      commit_message: COMMIT_MESSAGE,
    }

    if (options.authorName) {
      payload.author_name = options.authorName
    }
    if (options.authorEmail) {
      payload.author_email = options.authorEmail
    }
    debug('payload: ', payload)

    await api.put(url, payload)
    process.stdout.write('Done.\n')
  } catch (error) {
    if (error.response && error.response.status === 400) {
      debug('Error pushing: ', error)
      process.stdout.write(
        'Push failed with status code 400. File not modified?\n'
      )
      exit()
    } else {
      exit('Push failed', error)
    }
  }
}

;(async () => {
  DEBUG = !!options.debug
  FILE_NAME = options.fileName
  BASE_URL = options.url || BASE_URL

  // If we're not inside a local runner
  if (DEBUG || process.env.CI_PROJECT_ID !== '0') {
    if (process.env.GL_PRIVATE_TOKEN !== undefined) {
      CI_VARS.projectId = process.env.CI_PROJECT_ID
      CI_VARS.projectDir = process.env.CI_PROJECT_DIR
      CI_VARS.branch = process.env.CI_COMMIT_BRANCH
      CI_VARS.token = process.env.GL_PRIVATE_TOKEN
      debug('CI_VARS: ', CI_VARS)
    } else {
      exit('Error: required ENV variable GL_PRIVATE_TOKEN undefined.')
    }
  }

  if (options.branch !== 'CI_COMMIT_BRANCH') {
    BRANCH = options.branch
  } else {
    BRANCH = CI_VARS.branch
  }
  if (options.message !== 'Update <filename> [skip ci]') {
    COMMIT_MESSAGE = options.message
  } else {
    COMMIT_MESSAGE = `Update ${FILE_NAME} [skip ci]`
  }
  if (options.failOnError) {
    EXIT_CODE = 1
  }

  api.defaults.baseURL = BASE_URL
  api.defaults.headers.common['Private-Token'] = CI_VARS.token
  debug('api.defaults: ', api.defaults)

  pushFile()
})()
