# Push To Repo

Commit changes back to the repo from a GitLab CI job.

Uses GitLab API and by default fails quietly (if the pushed file is for example unchanged). Easy to use and flexible to configure.

## Configuration

1. Add `GL_PRIVATE_TOKEN` to your GitLab repo CI/CD variables with a valid GitLab access token as its value.

## Usage

`npx push-to-repo -h`

All options except the filename are optional. Note that by default the script does not set author details to the commit which means that it will use the credentials of the user who created the access token. This means that the commits will show up in that users commit history etc. If you want to avoid this, set author name and email explicitly.

```
Usage: push-to-repo -f <file> [options]

Options:
  -f, --file-name <filename>  the file to push
  -b, --branch <branch>       the branch to push (default: "CI_COMMIT_BRANCH")
  -m, --message <message>     commit message (default: "Update <filename> [skip ci]")
  -u, --base-url <url>        GitLab API base URL (default: "https://gitlab.com/api/v4")
  -n, --author-name           Author name
  -e, --author-email          Author e-mail
  --fail-on-error             fail the job on error
  -d, --debug                 debug (verbose) mode
  -V, --version               output the version number
  -h, --help                  display help for command
```

## Testing locally

This tool is intended to be used inside GitLab CI but you can test or use it locally by setting the following GitLab CI env variables: `CI_PROJECT_ID`, `CI_PROJECT_DIR`, and `CI_COMMIT_BRANCH`.
## Contributing

All contributions are welcome! Please follow the [code of conduct](https://www.contributor-covenant.org/version/2/0/code_of_conduct/) when interacting with others. [This project lives on GitLab](https://gitlab.com/uninen/push-to-repo).

[Follow @Uninen](https://twitter.com/uninen) on Twitter.
