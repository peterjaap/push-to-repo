# Changelog

## v0.1.0 - 2020-01-20

- Feature: added `--author-name` and `--author-email`.
- Fix: allow local testing.
- Docs: added more documentation.
- Chore: upgraded deps.
- Refactor: general cleanup and fixes.
## v0.0.5 - 2020-12-26

- Fix: properly URL encode file path.
## v0.0.3 - 2020-12-26

- Fix: added better debug info.
## v0.0.2 - 2020-12-16

- Fix: bugfixes.

## v0.0.1 - 2020-12-16

- Initial version.
